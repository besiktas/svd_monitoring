from typing import Dict, Optional
import torch

import json
from pathlib import Path


min_max_scaler = lambda t: (t - t.min()) / (t.max() - t.min())
max_scaler = lambda t: t / t.max()


def scale_acts(arg: str):
    _scale_acts = {"minmax": min_max_scaler, "max": max_scaler, None: None}
    return _scale_acts[arg]


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class RunInfo:
    def __init__(self, **kwargs) -> None:
        for key, val in kwargs.items():
            setattr(self, key, val)

    @classmethod
    def _default_run_info(cls, model_name: str, dataset_name: str, run_dir: str):
        return cls(model_name=model_name, dataset_name=dataset_name, model_check_points={}, run_dir=run_dir)


class ExperimentInfo:
    """helper class that"""

    def __init__(
        self, shuffled_targets: bool, model_name: str, dataset_name: str, base_run_dir: str = "runs/"
    ):
        self.shuffle_targets = "shuffled" if shuffled_targets else "normal"
        self.model_name = model_name.lower()
        self.dataset_name = dataset_name.lower()

        self.base_run_dir = Path(base_run_dir)

        self.run_dir = self.base_run_dir / self.dataset_name / self.model_name / self.shuffle_targets
        self.run_dir.mkdir(parents=True, exist_ok=True)

        self.json_file = self.run_dir / "info.json"
        # self.load_json()
        self.run_info = self._default_run_info()

    def _default_run_info(self):
        return {
            "model": self.model_name,
            "dataset": self.dataset_name,
            "model_checkpoints": {},
            "run_dir": str(self.run_dir.resolve()),
        }

    def _save_json(self):
        with open(self.json_file, "w+", encoding="utf-8") as file:
            json.dump(self.run_info, file, ensure_ascii=False, indent=4)

    def load_json(self) -> Optional[Dict]:
        try:
            with open(self.json_file, "r", encoding="utf-8") as file:
                json_data = json.load(file)
            return json_data
        except ValueError:
            return None

    def get_run_json(self) -> None:
        run_json = self.load_json()
        self.run_info = run_json if run_json else self._default_run_info()

    def save(self):
        self._save_json()

    def save_obj(self, key, obj):
        setattr(self, key, obj)
        save_path = self.run_dir / key
        save_path = str(save_path.resolve())
        self.run_info[key] = save_path
        torch.save(obj, save_path)

    def epoch_end(self, model, **kwargs):
        pass

    def save_epoch(self, model: torch.nn.Module, epoch: int):
        save_path = self.run_dir / f"{epoch}.pt"
        save_path = save_path.resolve()
        torch.save(model, save_path)
        self.run_info["model_checkpoints"][epoch] = str(save_path)
