import math
import copy

import torch
import torch.nn as nn
import torch.nn.functional as F


class MLP_(nn.Module):
    def __init__(self, n_units, init_scale=1.0):
        super(MLP_, self).__init__()

        self._n_units = copy.copy(n_units)
        self._layers = []
        for i in range(1, len(n_units)):
            layer = nn.Linear(n_units[i - 1], n_units[i], bias=False)
            variance = math.sqrt(2.0 / (n_units[i - 1] + n_units[i]))
            layer.weight.data.normal_(0.0, init_scale * variance)
            self._layers.append(layer)

            name = "fc%d" % i
            if i == len(n_units) - 1:
                name = "fc"  # the prediction layer is just called fc
            self.add_module(name, layer)

    def forward(self, x):
        x = x.view(-1, self._n_units[0])
        out = self._layers[0](x)
        for layer in self._layers[1:]:
            out = F.relu(out)
            out = layer(out)
        return out


class MLP(nn.Module):
    def __init__(self, n_classes: int = 10, bias: bool = False):
        super(MLP, self).__init__()

        self.fc1 = nn.LazyLinear(1028, bias=bias)
        self.relu1 = nn.ReLU()
        self.fc2 = nn.LazyLinear(1028, bias=bias)
        self.relu2 = nn.ReLU()
        self.out = nn.LazyLinear(n_classes, bias=bias)

        self.capture_layers = (self.relu1, self.relu2)

    def forward(self, x: torch.Tensor):
        x = x.view(len(x), -1)

        x = self.fc1(x)
        x = self.relu1(x)
        x = self.fc2(x)
        x = self.relu2(x)
        return self.out(x)


class CNN(nn.Module):
    """Basic CNN architecture."""

    def __init__(self, in_channels: int = 1, n_classes: int = 10):
        super(CNN, self).__init__()

        self.conv1 = nn.LazyConv2d(32, 5, 1)
        self.relu1 = nn.ReLU()

        self.conv2 = nn.LazyConv2d(32, 5, 2)
        self.relu2 = nn.ReLU()

        self.conv3 = nn.LazyConv2d(64, 3, 1)
        self.relu3 = nn.ReLU()

        self.flatten = nn.Flatten()

        self.fc1 = nn.LazyLinear(256)
        self.fc2 = nn.LazyLinear(n_classes)

        self.capture_layers = (self.relu1, self.relu2, self.relu3)

    def forward(self, x):

        x = self.conv1(x)
        x = self.relu1(x)

        x = self.conv2(x)
        x = self.relu2(x)

        x = self.conv3(x)
        x = self.relu3(x)

        x = self.flatten(x)

        x = self.fc1(x)
        x = self.fc2(x)
        return x


class PyNet(nn.Module):
    """CNN architecture. This is the same MNIST model from pytorch/examples/mnist repository"""

    def __init__(self, in_channels=1, n_classes: int = 10):
        super(PyNet, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(9216, 128)
        self.fc2 = nn.Linear(128, n_classes)

        self.flatten = nn.Flatten()

        self.relu1 = nn.ReLU()
        self.relu2 = nn.ReLU()
        self.relu3 = nn.ReLU()

        self.capture_layers = (self.relu1, self.relu2, self.relu3)

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu1(x)
        x = self.conv2(x)
        x = self.relu2(x)
        x = F.max_pool2d(x, 2)
        x = self.dropout1(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.relu3(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output
