from typing import List, Optional

import cka
import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torchvision.transforms.functional as F
import tqdm
import typer
from sklearn.decomposition import PCA, FastICA
from tellem import CaptureManager as CapMan
from tellem import DataLoaders, Perturber
from torch.utils.tensorboard import SummaryWriter

from models import MLP
from models_wideresnet import WideResNet
from utils.data_helpers import AverageMeter, ExperimentInfo

data_root = "/data/graham/svd_monitoring/data/"
model_checkpoint_roots = "/data/graham/svd_monitoring/cifar10/model_checkpoints"
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

app = typer.Typer()

writer = SummaryWriter("tensorboard_related/cifar10")

# from dataset.data.mean((0,1,2)) / 255.0
MEAN_ = torch.Tensor([0.4914, 0.4822, 0.4465])
STD_ = torch.Tensor([0.247, 0.2435, 0.2616])



class CIFAR10Patched(datasets.CIFAR10):
    def __init__(self, shuffle_targets: bool = False, **kwargs):
        super(CIFAR10Patched, self).__init__(**kwargs)

        self.n_classes = len(self.classes)

        if shuffle_targets:
            print("shuffling targets...")
            self.corrupt_labels(1.0)

    def corrupt_labels(self, corrupt_prob):
        labels = np.array(self.targets)
        np.random.seed(12345)
        mask = np.random.rand(len(labels)) <= corrupt_prob
        rnd_labels = np.random.choice(self.n_classes, mask.sum())
        labels[mask] = rnd_labels
        # we need to explicitly cast the labels from npy.int64 to
        # builtin int type, otherwise pytorch will fail...
        labels = [int(x) for x in labels]

        self.targets = labels

    def drop_class(self, c):
        keep = [i for i, v in enumerate(self.targets) if v != c]
        self.data = self.data[keep]
        self.targets = [self.targets[i] for i in keep]

    def keep_classes(self, keep_classes: List[int]):
        keep = [i for i, v in enumerate(self.targets) if v in keep_classes]
        self.data = self.data[keep]
        self.targets = [self.targets[i] for i in keep]


def load_data(
    shuffle_targets: bool = False,
    data_augmentation: bool = False,
    batch_size: int = 128,
    drop: List[int] = None,
):
    normalize = transforms.Normalize(mean=MEAN_, std=STD_)

    transform_train = transforms.Compose(
        [
            transforms.ToTensor(),
            normalize,
        ]
    )
    transform_test = transforms.Compose(
        [
            transforms.ToTensor(),
            normalize,
        ]
    )

    if data_augmentation:
        transform_train.transforms.insert(
            0,
            [
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
            ],
        )

    train_dataset = CIFAR10Patched(
        root=data_root,
        shuffle_targets=shuffle_targets,
        train=True,
        transform=transform_train,
        download=True,
    )

    test_dataset = CIFAR10Patched(
        root=data_root,
        shuffle_targets=False,
        train=False,
        transform=transform_test,
        download=True,
    )

    if drop:
        for c in drop:
            train_dataset.drop_class(c)
            test_dataset.drop_class(c)

    dataloaders = DataLoaders(
        train=torch.utils.data.DataLoader(
            train_dataset, batch_size=batch_size, shuffle=True, num_workers=1, pin_memory=True
        ),
        test=torch.utils.data.DataLoader(
            test_dataset, batch_size=batch_size, shuffle=False, num_workers=1, pin_memory=True
        ),
    )
    return dataloaders


def adjust_learning_rate(optimizer, epoch, learning_rate):
    adjusted_lr = learning_rate * (0.1 ** (epoch // 150)) * (0.1 ** (epoch // 255))

    for param_group in optimizer.param_groups:
        param_group["lr"] = adjusted_lr


def train_step(
    epoch: int,
    model: torch.nn.Module,
    dataloader: torch.utils.data.DataLoader,
    criterion: torch.nn.Module,
    optimizer: torch.optim.Optimizer,
    **kwargs,
):
    model.train()
    correct = 0
    for batch_idx, (data, labels) in enumerate(dataloader):
        data, labels = data.to(device), labels.to(device)

        output = model(data)
        loss = criterion(output, labels)

        _, pred_class = torch.max(output, 1)
        correct += (pred_class == labels).sum()

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print(f"EPOCH: {epoch} loss: {loss.item():.4f} acc: {correct/len(dataloader.dataset):.3f}")
    return {"loss": loss, "correct": correct}


def test_step(
    model: torch.nn.Module, dataloader: torch.utils.data.DataLoader, criterion: torch.nn.Module, **kwargs
):
    model.eval()
    correct = 0
    for batch_idx, (data, labels) in enumerate(dataloader):
        data, labels = data.to(device), labels.to(device)
        output = model(data)
        loss = criterion(output, labels)

        _, pred_class = torch.max(output, 1)

        correct += (pred_class == labels).sum()

    print(f"TEST/VAL=>loss: {loss.item():.4f} acc:{correct/len(dataloader.dataset):.3f}")
    return {"loss": loss, "correct": correct}


@app.command()
def train(
    shuffle_targets: bool = False,
    batch_size: int = 128,
    num_epochs: int = 100,
    run_test_step: bool = False,
    learning_rate: float = 0.1,
):
    # cudnn.benchmark = True
    dataloaders = load_data(shuffle_targets=shuffle_targets, batch_size=batch_size)

    model = WideResNet(
        depth=28, num_classes=dataloaders.train.dataset.n_classes, widen_factor=1, drop_rate=0.0
    )
    model.to(device)

    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, weight_decay=1e-4, momentum=0.9)
    loss_func = torch.nn.CrossEntropyLoss()

    train_info = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=shuffle_targets
    )

    print("Number of parameters: ", sum([p.data.nelement() for p in model.parameters()]))
    for epoch in range(num_epochs):
        adjust_learning_rate(optimizer, epoch, learning_rate)

        train_step(epoch, model, dataloaders.train, loss_func, optimizer)

        if run_test_step:
            test_step(model, dataloaders.test, loss_func)

        train_info.save_epoch(model, epoch)

    train_info.save()


@app.command()
def pca_analysis(
    batch_size: int = 128,
    n_epochs: int = -1,
    num_perturbations: int = 32,
    perturbation_mean: float = 0.0,
    perturbation_std: float = 0.1,
    scale_acts: str = None,
    pca_n_components: int = 10,
    pca_center: bool = False,
    pca_n_iter: int = 10,
):
    def get_pca_vals(pca_vals, acts):
        for key, act in acts.items():
            if scale_acts:
                act = scale_acts(act)

            vals = torch.pca_lowrank(
                act.view(num_perturbations, -1), q=pca_n_components, center=pca_center, niter=pca_n_iter
            )[1]

            pca_vals[key].append(vals.cpu().numpy())

    def get_pca_vals_sklearn(singular_values, exp_var, exp_var_ratio, acts, writer_tag, epoch):

        for key, act in acts.items():
            pca = PCA(n_components=pca_n_components, whiten=pca_center, iterated_power=pca_n_iter).fit(
                act.view(num_perturbations, -1).cpu()
            )

            singular_values[key].append(pca.singular_values_)
            exp_var[key].append(pca.explained_variance_)
            exp_var_ratio[key].append(pca.explained_variance_ratio_)

            writer.add_scalars(
                f"{writer_tag}/{key}",
                {
                    "svd": pca.singular_values_[0],
                    "explained_variance": pca.explained_variance_[0],
                    "explained_variance_ratio": pca.explained_variance_ratio_[0],
                },
                epoch,
            )

    # capture_normal = CaptureManager(model_normal)
    # capture_shuffled = CaptureManager(model_shuffled)

    # capture_normal.capture_layers_of_type(torch.nn.ReLU)
    # capture_normal.capture(capture_types=[torch.nn.Relu])
    # capture_shuffled.capture_layers_of_type(torch.nn.ReLU)
    #     capture_normal.capture()
    #     capture_shuffled.capture()

    #     cka_vals_idx = torch.zeros((len(capture_normal.captures)), len(capture_shuffled.captures))

    #     # breakpoint()

    #     cka_ = cka.CKAHelper(capture_normal.captures, capture_shuffled.captures)
    #     with capture_normal as cap_norm, capture_shuffled as cap_shuf:

    #         # if idx == 0:
    #         #     torch.save(acts_normal, "acts_normal.pt")
    #         #     torch.save(acts_shuffled, "acts_shuffled.pt")

    #         acts_normal, _ = cap_norm(data1)
    #         acts_shuffled, _ = cap_shuf(data1)
    #         cka_.minibatch(acts_normal, acts_shuffled, 2)

    #         acts_normal, _ = cap_norm(data2)
    #         acts_shuffled, _ = cap_shuf(data2)
    #         breakpoint()
    #         cka_.minibatch(acts_normal, acts_shuffled, 2)

    #         cka_.done()

    #         for i, (layer_i, acts_i) in enumerate(acts_normal.items()):
    #             for j, (layer_j, acts_j) in enumerate(acts_shuffled.items()):

    #                 cka_vals.append(
    #                     {
    #                         "epoch": idx,
    #                         "layer_i": layer_i,
    #                         "layer_j": layer_j,
    #                         "cka_val": cka.cka(acts_i, acts_j).item(),
    #                     }
    #                 )

    #         #         cka_vals_idx[i, j] = cka.cka(acts_i, acts_j)

    #         # cka_vals.append(cka_vals_idx)

    # # cka_vals = torch.stack(cka_vals)
    # # import pandas as pd
    # torch.save(cka_vals, "cka_vals.pt")

    # print(f"shape of item: {acts_normal['block1.layer.0.relu1'].view(64, -1).cpu().numpy().T.shape}")
    # layer1 = "block1.layer.0.relu1"
    # layer2 = "block3.layer.0.relu1"
    # layer_2 = ""

    # pca_vals_normal = torch.pca_lowrank(acts_normal[layer_].flatten(1))
    # pca_vals_shuffled = torch.pca_lowrank(acts_shuffled[layer_].flatten(1))

    # pca_vals_sn = PCA().fit(acts_normal[layer_].flatten(1).cpu().numpy())
    # pca_vals_ss = PCA().fit(acts_shuffled[layer_].flatten(1).cpu().numpy())
    # breakpoint()

    # cka_pca = cca.CKA_PCA()
    # outval1 = cka_pca.get_cka(acts_normal[layer1], acts_shuffled[layer2])
    # outval2 = cka_pca.get_cka(acts_normal[layer1], acts_normal[layer2])
    # outval3 = cka_pca.get_cka(acts_normal[layer1], acts_normal[layer1])
    # breakpoint()

    # cka = cca.cka(acts_normal[layer_], acts_shuffled[layer_])
    # cka_mb = cca.cka_minibatch(acts_normal[layer_], acts_shuffled[layer_])
    # breakpoint()

    # k_ = acts_normal[layer_].view(num_perturbations, -1)
    # k_ = k_ @ k_.T
    # p1 = k_.shape[1]

    # h_ = torch.eye(p1) - 1/p1 * (torch.ones(p1, p1))
    # k_p = h_ @ k_ @ h_
    # breakpoint()
    # u, s, v = cca.get_cca_similarity(acts_normal[layer_], acts_shuffled[layer_])
    # svcca_results = cca_np.get_cca_similarity(
    #     acts_normal["block1.layer.0.relu1"].view(num_perturbations, -1).cpu().numpy(),
    #     acts_shuffled["block1.layer.0.relu1"].view(num_perturbations, -1).cpu().numpy(),
    #     epsilon=1e-10,
    #     verbose=False,
    # )

    # if idx in [0, 33, 66, 99]:
    #     arr = np.zeros((len(acts_normal), len(acts_shuffled)))

    #     for ii, (key_normal, layer_normal) in enumerate(acts_normal.items()):
    #         for jj, (key_shuffled, layer_shuffled) in enumerate(acts_shuffled.items()):
    #             # if layer_normal.shape != layer_shuffled.shape:
    #             #     breakpoint()
    #             breakpoint()
    #             vals = cca_np.torch_wrapper(cca_np.compute_pwcca, layer_normal, layer_shuffled)

    #             arr[ii, jj] = vals[0]

    #     pwcca_vals.append(arr)

    # vals = cca_np.torch_wrapper(cca_np.compute_pwcca, acts_normal[layer_], acts_shuffled[layer_])

    # cov = np.cov(acts_normal[layer_].view(num_perturbations, -1).cpu(), acts_shuffled[layer_].view(num_perturbations, -1).cpu())
    # cov2 = torch.cov(
    #     torch.concat(
    #         (
    #             acts_normal[layer_].view(num_perturbations, -1),
    #             acts_shuffled[layer_].view(num_perturbations, -1),
    #         )
    #     )
    # )

    # breakpoint()

    # svcca_results = get_cca_similarity(
    #     acts_normal["block1.layer.0.relu1"].view(64, -1).cpu().numpy(),
    #     acts_shuffled["block1.layer.0.relu1"].view(64, -1).cpu().numpy(),
    #     epsilon=1e-10,
    #     verbose=False,
    # )

    # breakpoint()

    # svcca_results = get_cca_similarity(acts_normal, acts_shuffled, epsilon=1e-10, verbose=False)
    # svcca_results = get_cca_similarity(
    #     acts_normal["block1.layer.0.relu1"].view(64, -1).cpu().numpy().T,
    #     acts_shuffled["block1.layer.0.relu1"].view(64, -1).cpu().numpy().T,
    #     epsilon=1e-10,
    #     verbose=False,
    # )

    # get_pca_vals(pca_vals_normal, acts_normal)
    # get_pca_vals_sklearn(
    #     _singular_values_normal, _exp_var_normal, _exp_var_ratio_normal, acts_normal, "normal", idx
    # )

    # # breakpoint()
    # get_pca_vals(pca_vals_shuffled, acts_shuffled)
    # get_pca_vals_sklearn(
    #     _singular_values_shuffled,
    #     _exp_var_shuffled,
    #     _exp_var_ratio_shuffled,
    #     acts_shuffled,
    #     "shuffled",
    #     idx,
    # )

    # torch.save(pwcca_vals, "pwcca.pt")

    # train_info_normal.save_obj("pca_vals", pca_vals_normal)
    # train_info_shuffled.save_obj("pca_vals", pca_vals_shuffled)

    # # SKLEARN VALS
    # train_info_normal.save_obj("svd_vals", _singular_values_normal)
    # train_info_shuffled.save_obj("svd_vals", _singular_values_shuffled)

    # train_info_normal.save_obj("exp_var", _exp_var_normal)
    # train_info_shuffled.save_obj("exp_var", _exp_var_shuffled)

    # train_info_normal.save_obj("exp_var_ratio", _exp_var_ratio_normal)
    # train_info_shuffled.save_obj("exp_var_ratio", _exp_var_ratio_shuffled)

    # train_info_normal.save()
    # train_info_shuffled.save()
    # writer.close()


@app.command()
def analysis_cka_svd(
    batch_size: int = 256,
    num_perturbations: int = 32,
    perturbation_mean: float = 0.0,
    perturbation_std: float = 0.1,
    n_epochs: int = -1,
    # savefile: str = "runs/cifar10/wideresnet/cka_svd.pt",
    symmetric: bool = False,
):

    train_info_normal = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=False
    )
    train_info_shuffled = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=True
    )

    dataloaders = load_data(shuffle_targets=True, batch_size=batch_size)

    # model = WideResNet(
    #     depth=28, num_classes=dataloaders.train.dataset.n_classes, widen_factor=1, drop_rate=0.0
    # )
    # model.to(device)

    train_info_normal.get_run_json()
    train_info_shuffled.get_run_json()

    perturber = Perturber(
        dist=torch.distributions.normal.Normal(loc=perturbation_mean, scale=perturbation_std),
        num_perturbations=num_perturbations,
    )

    n_epochs = len(train_info_normal.run_info["model_checkpoints"]) if n_epochs < 0 else n_epochs

    cka_svd = cka.CKA_svd()
    cka_mb = cka.CKA_MiniBatch(perturber=perturber, device=device)

    iter_test = iter(dataloaders.test)
    iter_train = iter(dataloaders.train)

    n_samples_wanted = 4
    good_label = 0
    train_idxs = torch.where(torch.Tensor(dataloaders.train.dataset.targets) == good_label)[0][
        :n_samples_wanted
    ]

    test_idxs = torch.where(torch.Tensor(dataloaders.test.dataset.targets) == good_label)[0][
        :n_samples_wanted
    ]
    breakpoint()

    def _transform(data):
        data = torch.from_numpy(data).to(torch.float)
        data = data.permute(0, 3, 2, 1)
        data /= 255.0
        data = F.normalize(data, MEAN_, STD_)
        data = perturber.repeat(data, num_perturbations=num_perturbations, from_dim=1)
        return data

    data_train_0 = _transform(dataloaders.train.dataset.data[train_idxs])
    data_test_0 = _transform(dataloaders.train.dataset.data[test_idxs])

    data_test_0 = data_test_0.to(device)[:64]
    data_train_0 = data_train_0.to(device)[:64]

    train_vals = {}
    test_vals = {}
    # for epoch in tqdm.tqdm(range(1, n_epochs), position=0):

    for epoch in range(1, n_epochs):
        print(f"on epoch: {epoch}")

        model_curr = torch.load(train_info_normal.run_info["model_checkpoints"][str(epoch)])
        model_prev = torch.load(train_info_normal.run_info["model_checkpoints"][str(epoch - 1)])

        # breakpoint()
        # cka_svd.make_svd()

        # cka_mb.capture_1 = CapMan(model_curr, return_preds=False).capture(keep_activations=False)
        # cka_mb.capture_2 = CapMan(model_prev, return_preds=False).capture(keep_activations=False)
        capture1 = CapMan(model_curr, return_preds=False).capture(keep_activations=False)
        capture2 = CapMan(model_prev, return_preds=False).capture(keep_activations=False)

        # data_enum = tqdm.tqdm(dataloaders.test, position=1)
        # cka_mb.batch(data_enum, epoch)

        # acts = capture1(data_train_0)
        # capture1 = None

        acts1 = capture1(data_train_0)

        capture1.detach()

        capture1.attach()
        acts2 = capture1(data_train_0)

        breakpoint()

        # with capture1 as cap1:
        #     acts = cap1(data_test_0)

        # with capture1.acts() as capture:
        #     print("in")
        # with capture1 as cap:
        #     out_ = cap(data_train_0)
        #     print("in ")
        # breakpoint()

        print("out ")

        # train_vals[epoch] = cka_svd.batch(data_train_0, capture1, capture2)
        # test_vals[epoch] = cka_svd.batch(data_test_0, capture1, capture2)

    # torch.save(train_vals, "runs/cifar10/wideresnet/epoch_cka_svd_perturbed_train.pt")
    # torch.save(test_vals, "runs/cifar10/wideresnet/epoch_cka_svd_perturbed_test.pt")

    # torch.save(cka_mb.epoch_hsic_matrix, "runs/cifar10/wideresnet/epoch_hsic_cka.pt")
    # torch.save(cka_mb.epoch_hsic_matrix_svd, "runs/cifar10/wideresnet/epoch_hsic_cka_svd.pt")


@app.command()
def analysis_cka_svd_post(savefile: str = "cka_svd.pt", all_combos: bool = False):
    cka_svd: cka.CKA_svd = torch.load(savefile)

    for model_name in ["normal", "shuffled"]:

        # model_name = "normal"

        num_epochs = len(cka_svd.vals[model_name])

        for epoch in tqdm.trange(num_epochs):
            cka_svd.compute_cka_prev(model_name, epoch, all_combos=all_combos)

    torch.save(cka_svd, savefile)


@app.command()
def analysis(
    batch_size: int = 128,
    n_epochs: int = -1,
    drop: Optional[List[int]] = typer.Option(None),
    num_perturbations: int = 32,
    perturbation_mean: float = 0.0,
    perturbation_std: float = 0.1,
    scale_acts: str = None,
    name_extra: str = "",
    pca_n_components: int = 10,
    pca_center: bool = False,
    pca_n_iter: int = 10,
):

    dataloaders = load_data(shuffle_targets=True, batch_size=batch_size, drop=drop)

    # model = WideResNet(
    #     depth=28, num_classes=dataloaders.train.dataset.n_classes, widen_factor=1, drop_rate=0.0
    # )
    # model.to(device)

    train_info_normal = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=False
    )
    train_info_shuffled = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=True
    )

    train_info_normal.get_run_json()
    train_info_shuffled.get_run_json()

    perturber = Perturber(
        dist=torch.distributions.normal.Normal(loc=perturbation_mean, scale=perturbation_std),
        num_perturbations=num_perturbations,
    )

    n_epochs = len(train_info_normal.run_info["model_checkpoints"]) if n_epochs < 0 else n_epochs

    cka_all_minibatches = {}
    cka_all_perturb = {}

    for epoch_idx in tqdm.trange(n_epochs, position=0, desc="==>epoch"):

        model_normal = torch.load(train_info_normal.run_info["model_checkpoints"][str(epoch_idx)])
        model_shuffled = torch.load(train_info_shuffled.run_info["model_checkpoints"][str(epoch_idx)])

        cka_epoch = cka.CKA(model_normal, model_shuffled)
        cka_epoch.compare_hsic(dataloaders.test, perturber=perturber)

        cka_all_minibatches[epoch_idx] = cka_epoch.hsic_matrix
        cka_all_perturb[epoch_idx] = cka_epoch.hsic_matrix_perturbed

    torch.save(cka_all_minibatches, f"cka_minibatches{name_extra}.pt")
    torch.save(cka_all_perturb, f"cka_perturb{name_extra}.pt")


@app.command()
def plot_pca():

    train_info_normal = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=False
    )
    train_info_shuffled = ExperimentInfo(
        model_name="wideresnet", dataset_name="cifar10", shuffled_targets=True
    )

    train_info_normal.get_run_json()
    train_info_shuffled.get_run_json()

    pca_vals_normal = torch.load(train_info_normal.run_info["pca_vals"])
    pca_vals_shuffled = torch.load(train_info_shuffled.run_info["pca_vals"])

    svd_vals_normal = torch.load(train_info_normal.run_info["svd_vals"])
    svd_vals_shuffled = torch.load(train_info_shuffled.run_info["svd_vals"])

    exp_var_normal = torch.load(train_info_normal.run_info["exp_var"])
    exp_var_shuffled = torch.load(train_info_shuffled.run_info["exp_var"])

    exp_var_ratio_normal = torch.load(train_info_normal.run_info["exp_var_ratio"])
    exp_var_ratio_shuffled = torch.load(train_info_shuffled.run_info["exp_var_ratio"])

    def fix_vals(vals_dict):
        for key, arr in vals_dict.items():
            if isinstance(arr[0], np.ndarray):
                vals_dict[key] = np.array(arr)
            else:
                vals_dict[key] = torch.stack(arr).cpu().numpy()

    fix_vals(pca_vals_normal)
    fix_vals(pca_vals_shuffled)

    fix_vals(svd_vals_normal)
    fix_vals(svd_vals_shuffled)
    fix_vals(exp_var_normal)
    fix_vals(exp_var_shuffled)
    fix_vals(exp_var_ratio_normal)
    fix_vals(exp_var_ratio_shuffled)

    plot_dir = "plots/"

    def _plot(vals_normal, vals_shuffled, plot_name):
        fig_all, ax_all = plt.subplots(nrows=2, ncols=2, figsize=(26, 16))

        for key, arr in vals_normal.items():
            # 1st row - 1st singular value
            ax_all[0, 0].set_title("normal")
            ax_all[0, 0].plot(arr[:, 0], label=key)

            # 2nd row - 2nd singular value
            ax_all[1, 0].plot(arr[:, 1], label=key)

        for key, arr in vals_shuffled.items():
            ax_all[0, 1].set_title("shuffled")
            ax_all[0, 1].plot(arr[:, 0], label=key)

            ax_all[1, 1].plot(arr[:, 1], label=key)

        colormap = plt.cm.gist_ncar
        colors = [colormap(i) for i in np.linspace(0, 1, len(ax_all[0, 0].lines))]

        def fix_colors(ax):
            for i, j in enumerate(ax.lines):
                j.set_color(colors[i])
            j.set_color("black")

        fix_colors(ax_all[0, 0])
        fix_colors(ax_all[0, 1])
        fix_colors(ax_all[1, 0])
        fix_colors(ax_all[1, 1])

        ax_all[0, 1].legend(loc="center left", bbox_to_anchor=(1, 0.5))
        fig_all.tight_layout()
        fig_all.savefig(f"{plot_dir}/all-layers-{plot_name}.png")

    # _plot(pca_vals_normal, pca_vals_shuffled, "pca")
    _plot(svd_vals_normal, svd_vals_shuffled, "svd")
    _plot(exp_var_normal, exp_var_shuffled, "exp_var")
    _plot(exp_var_ratio_normal, exp_var_ratio_shuffled, "exp_var_ratio")
    # _plot(ica_vals_normal, ica_vals_shuffled, "ica")


if __name__ == "__main__":
    app()
    # mp.set_start_method("spawn")
    # cka_svd_obj: cka.CKA_svd = torch.load("cka_svd.pt")

    # # default_process(cka_svd_obj)
    # mp_process(cka_svd_obj)

    # model_name = "normal"

    # num_epochs = len(cka_svd.vals[model_name])

    # with mp.Pool(1) as p:
    #     for epoch in tqdm.trange(1, num_epochs):
    #         for layer_name, layer in cka_svd.vals[model_name][epoch].items():
    #             prev_layer = cka_svd.vals[model_name][epoch - 1][layer_name]
    #             d = {"u1": layer["u"], "s1": layer["s"], "u2": prev_layer["u"], "s2": prev_layer["s"]}
    #             results = p.map(_cka_u_s, (d,))

    # print(f"doing fast: {fast}")
    # for epoch in tqdm.trange(num_epochs):
    # if fast:
    #
    # else:
    # cka_svd.compute_cka_prev(model_name, epoch)
