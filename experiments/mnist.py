from typing import Any, Callable, Optional, Tuple
import torch
import torch.optim as optim
import torchvision

from tellem.utils import EasyDict
from tellem import DataLoaders, CaptureManager, Perturber, TrainerHelper

from functools import partial
import types
from models import CNN, MLP

config = EasyDict(dataroot="/data/graham/svd_monitoring/data/mnist")

batch_size = 32


class MNISTPatched(torchvision.datasets.MNIST):
    """tried to monkeypatch but it didnt work so doing this instead"""

    def __init__(self, shuffle_targets: bool = False, **kwargs) -> None:
        super().__init__(**kwargs)

        # self._shuffle_targets = shuffle_targets

        self.shuffled_targets = self.targets[torch.randperm(len(self.targets))] if shuffle_targets else False

    def __getitem__(self, idx: int):
        img, target = super().__getitem__(idx)

        if self.shuffled_targets is not False:
            return img, target, int(self.shuffled_targets[idx])

        return img, target


def load_datasets() -> EasyDict:

    mnist_info = EasyDict(in_channels=1, dataset=MNISTPatched, root=config.dataroot)
    train_transforms = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
    test_transforms = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])

    # Load MNIST dataset
    train_dataset = MNISTPatched(root=mnist_info.root, transform=train_transforms, download=True, shuffle_targets=True)
    test_dataset = MNISTPatched(root=mnist_info.root, train=False, transform=test_transforms, download=True)
    return EasyDict(train=train_dataset, test=test_dataset)


def _fit(self, num_epochs: int):
    for n_epoch in range(num_epochs):
        self.model.train()
        for idx, (inputs, labels) in enumerate(self.dataloaders["train"]):
            # inputs, labels = inputs.to(self.device), labels.to(self.device)
            inputs, labels = self.to_device(inputs, labels)


def main():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    mlp_layers = [28 * 28, 512, 512, 10]
    # model = MLP(mlp_layers).to(device)
    # model_shuffled = MLP(mlp_layers).to(device)
    model = CNN().to(device)
    model_shuffled = CNN().to(device)

    datasets = load_datasets()

    capture = CaptureManager(model)
    capture.capture_layers_of_type(torch.nn.ReLU)

    capture_shuffled = CaptureManager(model_shuffled)
    capture_shuffled.capture_layers_of_type(torch.nn.ReLU)

    # patch_getitem(datasets.train)
    perturber0_1 = Perturber(dist=torch.distributions.normal.Normal(loc=0, scale=0.1))
    perturber0_001 = Perturber(dist=torch.distributions.normal.Normal(loc=0, scale=0.001))

    # make it so we can use shuffled tagets

    dataloaders = DataLoaders(
        train=torch.utils.data.DataLoader(datasets.train, batch_size=batch_size, shuffle=True, num_workers=2),
        test=torch.utils.data.DataLoader(datasets.test, batch_size=batch_size, shuffle=False, num_workers=2),
    )
    make_optim = lambda m: optim.SGD(m.parameters(), lr=0.001, weight_decay=1e-4, momentum=0.9)
    # optimizer = optim.SGD(model.parameters(), lr=0.001, weight_decay=1e-4)
    # optimizer_shuffled = optim.SGD(model_shuffled.parameters(), lr=0.001, weight_decay=1e-4)
    optimizer, optimizer_shuffled = make_optim(model), make_optim(model_shuffled)

    loss_func = torch.nn.CrossEntropyLoss()
    num_epochs = 100

    for epoch in range(num_epochs):

        capture.detach()
        capture_shuffled.detach()

        model.train()
        model_shuffled.train()

        correct = 0.0
        correct_shuffled = 0.0

        for batch_idx, (data, labels, shuffled_labels) in enumerate(dataloaders["train"]):

            data, labels, shuffled_labels = data.to(device), labels.to(device), shuffled_labels.to(device)

            optimizer.zero_grad()

            output = model(data)

            loss = loss_func(output, labels)
            loss.backward()
            optimizer.step()
            _, predicted_class = torch.max(output, 1)
            correct += (predicted_class == labels).sum()

            # shuffled related
            optimizer_shuffled.zero_grad()
            output = model_shuffled(data)

            loss_shuffled = loss_func(output, shuffled_labels)
            loss_shuffled.backward()
            optimizer_shuffled.step()

            _, predicted_class = torch.max(output, 1)
            correct_shuffled += (predicted_class == labels).sum()

        print(f'Train Epoch: {epoch} Loss: {loss.item():.6f} | accuracy: {correct / len(dataloaders["train"].dataset):.3f}')
        print(
            f'Train Epoch: {epoch} Loss-Shuffled: {loss_shuffled.item():.6f} | accuracy-shuffled: {correct_shuffled / len(dataloaders["train"].dataset):.3f}'
        )

        # capture.attach()

        # data = data[0]

        # data = data.repeat(batch_size, *(1 for _ in data.shape))

        # acts, preds = capture(data + perturber0_1(data))

        # acts_ = acts["relu1"].view(len(data), -1)
        # pca_vals_centered = torch.pca_lowrank(acts_)
        # pca_vals_not_centered = torch.pca_lowrank(acts_, center=False)

        # print("got pca_vals - 0.001", pca_vals_centered[1], pca_vals_not_centered[1])

        # acts, preds = capture(data + perturber0_001(data))
        # acts_ = acts["relu1"].view(len(data), -1)
        # pca_vals_centered = torch.pca_lowrank(acts_)
        # pca_vals_not_centered = torch.pca_lowrank(acts_, center=False)

        # print("got pca_vals - 0.001", pca_vals_centered[1], pca_vals_not_centered[1])

        # acts, preds = capture(data + perturber0_01(data))
        # breakpoint()

    # trainer = TrainerHelper(model=model, dataloaders=dataloaders)
    # trainer.fit = partial(_fit, trainer)
    # trainer.fit(5)


if __name__ == "__main__":

    main()
