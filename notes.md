# notes on overfitting intentionally

- apparently MNIST is super hard to do this
  - https://github.com/pluskid/fitting-random-labels

```python
def eqn3_solve():
    # https://arxiv.org/pdf/1611.03530.pdf
    # X @ X.T @ coeffs = y
    import numpy as np

    datasets = load_datasets()
    X = datasets.train.data
    y = datasets.train.targets
    X = X.view(len(X), -1)

    n_vals = 1000
    X, y = X[0:n_vals], y[0:n_vals]
    X = X.T
    breakpoint()
    soln = np.linalg.solve(X @ X.T, y)

    output = X @ X.T @ soln

    correct = torch.isclose(output.long(), y, atol=1.0, rtol=0.0)
    print(f"correct - train: {correct.sum()/len(correct)}")

    X_test = datasets.test.data
    y_test = datasets.test.targets
    breakpoint()

    X_test = X_test.view(len(X_test), -1)
    X_test, y_test = X_test[0:n_vals], y_test[0:n_vals]

    output = X_test @ X_test.T @ soln

    correct = torch.isclose(output.long(), y_test, atol=1.0, rtol=0.0)
    print(f"correct - test: {correct.sum()/len(correct)}")
    # breakpoint()

    # datasets.train
    # breakpoint()
    # data = ...
```


```
In our papers (SVCCA, Insights on Representational Similarity), we explored some questions using these technqiues, but many other open questions remain:

Applying some of these methods to understand properties of generative models.
- Exploring low rank based compression, particularly with PLS
- Understanding learning dynamics and representational similarity of generalizing/memorizing networks through training.
- Identifying the important neurons in a layer, and determining how many there are. Relatedly, identifying which neurons are become sensitive to which classes.
- Pinpointing the effect of different kinds of layers on the representation.
- Comparing representations in biological and artificial neural networks.
```

Lets take this question
`Understanding learning dynamics and representational similarity of generalizing/memorizing networks through training.`

Lets look at what happens when we take a sample that is perturbed versus not perturbed for a for a network that is memorizing.

Since the network is memorizing, we might expect to see the 