# base image
FROM pytorch/pytorch:latest

# upgrades and installs pip
RUN apt-get -y update && apt-get -y install git

# RUN pip install --upgrade pip torch
RUN pip install "git+https://github.com/grahamannett/tellem.git"

COPY . .
# run the application
CMD ["python", "experiments/mnist.py"]