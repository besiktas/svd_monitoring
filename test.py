# import torch
# from sklearn.decomposition import PCA

# torch.manual_seed(0)


# def center(x):
#     # n = len(x)
#     # ones = torch.ones(n, 1)
#     # h =
#     std = x.std()
#     mean = x.mean()
#     x = x - mean
#     x = x / std
#     # x = (x - x.mean()) / x.std()
#     return x

# class A:
#     def __init__(self):
#         self.d = {}
#     def __setitem__(self, key, val):


# # y = torch.rand(64, 200)


# # pca_x = PCA().fit(x)
# # pca_y = PCA().fit(y)

# # sx = torch.from_numpy(pca_x.singular_values_)
# # vx = torch.from_numpy(pca_x.components_)

# # sy = torch.from_numpy(pca_y.singular_values_)
# # vy = torch.from_numpy(pca_y.components_)


# # vx = vx.T.abs()
# # vy = vy.T.abs()
# # x = torch.rand(64, 100)
# # y = x + torch.rand_like(x) * 0.001
# # # y = torch.rand(64, 200)


# # x = x @ x.T
# # y = y @ y.T
# # # x = center(x)
# # # y = center(y)

# # ux, sx, vx = torch.pca_lowrank(x)
# # uy, sy, vy = torch.pca_lowrank(y)
# # sx = torch.sqrt(sx)
# # sy = torch.sqrt(sy)


# # numer = 0.0
# # for i, sx_i in enumerate(sx):
# #     for j, sy_j in enumerate(sy):
# #         numer += (sx_i**2) * (sy_j**2) * (torch.dot(ux[i], uy[j])) ** 2


# # cka = numer / (torch.norm(sx) * torch.norm(sy))

# # breakpoint()
# # print(cka)


# x = torch.rand(64, 100)
# y = x + (torch.rand_like(x) * 0.001)

# ux, sx, vx = torch.linalg.svd(x)
# # ux, sx, vx = torch.svd_lowrank(x, q=64, niter=100)
# uy, sy, vy = torch.linalg.svd(y)

# # uy, sy, vy = torch.svd_lowrank(y, q=64, niter=100)


# # ux, sx, vx = torch.svd(x)
# # uy, sy, vy = torch.svd(y)

# # ux, sx, vx = torch.pca_lowrank(x, q=64, center=False)
# # uy, sy, vy = torch.pca_lowrank(y, q=64, center=False)

# num = 0
# for i, si in enumerate(sx):
#     for j, sj in enumerate(sy):
#         num += si * sj * torch.inner(ux[i], uy[j])

# cka = num / (torch.norm(sx) * torch.norm(sy))

# print(cka)
# # breakpoint()

from __future__ import annotations
from collections import defaultdict
import torch


class NestedDefaultDict(defaultdict):
    def __init__(self, *args, **kwargs):
        super(NestedDefaultDict, self).__init__(NestedDefaultDict, *args, **kwargs)

    def __repr__(self):
        return repr(dict(self))

    def to_dict(self):
        out = dict(self)
        for key, val in out.items():
            if isinstance(val, NestedDefaultDict):
                out[key] = val.to_dict()
        return out

    # @staticmethod
    # def to_dict(d: NestedDefaultDict):
    #     for key, val in d.items():
    #         if isinstance(val, NestedDefaultDict):
    #             return NestedDefaultDict.to_dict(dict(val))
    #         d[key] = val
    #     return d


a = NestedDefaultDict()

a[1]["a"] = 5
a[1]["b"] = 10

# print(a)

aa = a.to_dict()
# print(aa)

torch.save(a, "a.pt")
torch.save(aa, "aa.pt")

# a_ = torch.load("a.pt")
a_ = torch.load("aa.pt")
print(a_)
