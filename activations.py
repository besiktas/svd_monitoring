from typing import Any


class ActivationsGroup:
    def __init__(self, layer=None, model=None):
        self._layer = layer
        self._model = model

    def __call__(self, *args: Any, **kwds: Any) -> None:
        pass
