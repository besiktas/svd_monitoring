from tinydb import TinyDB, Query


class ExperimentManager:
    def __init__(self, db_path):
        self.db_path = db_path
        self.db = TinyDB(self.db_path)


if __name__ == "__main__":
    em = ExperimentManager("db.json")
    em.db.insert({"type": "apple", "count": 7})
