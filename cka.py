from collections import defaultdict
from functools import partial
from typing import Dict, Iterable, List, Sequence

import numpy as np
import torch
import torch.nn as nn
from sklearn.decomposition import PCA
from tellem import CaptureManager, Perturber
from tellem.utils import NestedDefaultDict
from torch.utils.data import DataLoader
from tqdm import tqdm


def centering_matrix(x: torch.Tensor):
    return torch.eye(len(x)) - (1 / len(x)) * torch.ones(len(x)) @ torch.ones(len(x)).T


def sim_matrix(k: torch.Tensor):
    n = len(k)
    h = torch.eye(n) - (1 / n) * torch.ones(n, 1) * torch.ones(n, 1).T
    h = h.to(k.device)
    return h @ k @ h


def tilde(X: torch.Tensor, center: bool = True):
    X = X.flatten(1)

    # unclear if center or center/std
    if center:
        X -= X.mean()

    K = X @ X.t()
    K.fill_diagonal_(0.0)
    return K


def hsic0(K, L):
    K_p = sim_matrix(K)
    L_p = sim_matrix(L)
    return torch.dot(K_p.flatten(), L_p.flatten()) / ((1 - len(K)) ** 2)


def hsic1(K: torch.Tensor, L: torch.Tensor):
    """
    Computes the unbiased estimate of HSIC metric.
    Reference: https://arxiv.org/pdf/2010.15327.pdf Eq (3)
    """
    N = K.shape[0]
    ones = torch.ones(N, 1).to(K.device)
    result = torch.trace(K @ L)
    result += ((ones.t() @ K @ ones @ ones.t() @ L @ ones) / ((N - 1) * (N - 2))).item()
    result -= ((ones.t() @ K @ L @ ones) * 2 / (N - 2)).item()
    return (1 / (N * (N - 3)) * result).item()


def cka_u_s(u1, s1, u2, s2, uneven: bool = False) -> torch.Tensor:
    # U = u1 @ u2.T

    if uneven:
        U = u1.T @ u2
    else:
        U = u1 @ u2.T
    # U = u1.T @ u2

    # if (s1.shape[0] < U.shape[0]) or (s2.shape[0] < U.shape[0]):
    # error probably the last layer
    # return 0.0

    num = (s1[:, None] @ s2[None, :]) * U

    return (num.sum() / (torch.norm(s1) * torch.norm(s2))).item()


def cka_u_s_slow(u1: torch.Tensor, s1: torch.Tensor, u2: torch.Tensor, s2: torch.Tensor) -> torch.Tensor:
    num = 0.0
    U = u1 @ u2.T
    for i, si in enumerate(s1):
        for j, sj in enumerate(s2):
            num += si * sj * U[i, j]
    return (num / (torch.norm(s1) * torch.norm(s2))).item()


class LayerStat:
    def __init__(self, layer):
        self.layer = layer


class CKA_svd:
    def __init__(self, model_names: Sequence[str] = None):
        self.cka_layer = {}

        self.vals = NestedDefaultDict()
        self.cka_vals = NestedDefaultDict()

        if model_names:
            for name in model_names:
                self.vals[name] = {}
                self.cka_vals = {}

    def make_saveable(self):
        self.vals = self.vals.to_dict()
        self.cka_vals = self.cka_vals.to_dict()

    def use_model(self, model: nn.Module, model_name: str):
        setattr(self, model_name, model)

    def for_epoch(
        self,
        epoch: int,
        act1_name: str,
        act2_name: str,
        activations1: Dict[str, torch.Tensor],
        activations2: Dict[str, torch.Tensor],
        symmetric: bool = True,
    ):

        for layer, act in activations1.items():
            act = act.flatten(1)
            if symmetric:
                act = act @ act.T
            u, s, _ = torch.svd(act)

            self.vals[act1_name][epoch][layer] = {"u": u, "s": s}

        for layer, act in activations2.items():
            act = act.flatten(1)
            if symmetric:
                act = act @ act.T
            u, s, _ = torch.svd(act)

            self.vals[act2_name][epoch][layer] = {"u": u, "s": s}

    def make_svd(self, acts: torch.Tensor, symmetric: bool = False):
        acts = acts.flatten(1)
        if symmetric:
            acts = acts @ acts.T
        return torch.svd(acts)

    def svd_for_layer_epoch(
        self, model_name: str, epoch: int, acts: Dict[str, torch.Tensor], symmetric: bool = False
    ):
        # saver = self.vals.get(model_name, {}) # not sure if i should make defaultdict

        for layer, act in acts.items():
            u, s, _ = self.make_svd(act)
            # act = act.flatten(1)
            # if symmetric:
            #     act = act @ act.T
            # u, s, _ = torch.svd(act)

            self.vals[model_name][epoch][layer] = {"u": u, "s": s}

    def compute_cka_prev(self, model_name: str, epoch: int, all_combos: bool = False):
        prev_epoch = epoch - 1
        if self.vals[model_name].get(prev_epoch, None) is None:
            print(f"no prev for epoch: {epoch}")
            return

        epoch_cka_vals = torch.zeros(len(self.vals[model_name][epoch]), len(self.vals[model_name][epoch]))

        combos = [
            [[i, x], [j, y]]
            for i, x in enumerate(self.vals[model_name][epoch].keys())
            for j, y in enumerate(self.vals[model_name][prev_epoch].keys())
        ]

        if not all_combos:
            combos = [[[i, x], [j, y]] for (i, x), (j, y) in combos if i == j]

        for (i, layer_name), (j, prev_layer_name) in combos:
            prev_layer = self.vals[model_name][prev_epoch][prev_layer_name]
            layer = self.vals[model_name][epoch][layer_name]
            _cka = cka_u_s(*prev_layer.values(), *layer.values())

            epoch_cka_vals[i, j] = _cka

        self.cka_vals.setdefault(model_name, {}).setdefault(epoch, {})
        self.cka_vals[model_name][epoch] = epoch_cka_vals

    def compute_cka_prev_fast_(self, model_name, epoch):
        prev_epoch = epoch - 1
        if self.vals[model_name].get(prev_epoch, None) is None:
            print(f"no prev for epoch: {epoch}")
            return

        self.cka_vals.setdefault(model_name, {}).setdefault(epoch, {})

    def batch(self, data, capture1: CaptureManager, capture2):
        acts1 = capture1(data)
        acts2 = capture2(data)
        both_symmetric = False

        vals = torch.zeros(len(capture1), len(capture2))
        for i, (layer_i, acts_i) in enumerate(acts1.items()):
            # print("on layer i", i)
            u1, s1, _ = self.make_svd(acts_i, symmetric=both_symmetric)
            for j, (layer_j, acts_j) in enumerate(acts2.items()):
                u2, s2, _ = self.make_svd(acts_j, symmetric=both_symmetric)

                # val = cka_u_s(u1, s1, u2, s2)
                vals[i, j] = cka_u_s(u1, s1, u2, s2, uneven=True)
        return vals


class CKA_MiniBatch:
    def __init__(self, perturber, device):
        # self.hsic_matrix = None
        self.perturber = perturber
        self.device = device

        self.capture_1: CaptureManager = None
        self.capture_2: CaptureManager = None

        self.epoch_hsic_matrix = {}
        self.epoch_hsic_matrix_svd = {}

    def batch_svd(
        self, data: torch.Tensor, label: torch.Tensor, epoch: int, num_batches: int, label_class: int = 0
    ):
        hsic_matrix_svd = self.epoch_hsic_matrix_svd.get(
            epoch, torch.zeros(len(self.capture_1), len(self.capture_2), 3)
        )
        data_idxs = label == label_class
        data_ = data[data_idxs]
        # label_ = label[data_idxs]

        data_ = self.perturber.repeat(data_, 20, 1)

        if len(data_) > len(data):
            data_ = data_[: len(data)]

        for i, (layer_i, acts_i) in enumerate(self.capture_1(data_).items()):
            K = tilde(acts_i)
            hsic_matrix_svd[i, :, 0] += hsic1(K, K) / num_batches
            for j, (layer_j, acts_j) in enumerate(self.capture_2(data_).items()):
                L = tilde(acts_j)
                hsic_matrix_svd[i, j, 1] += hsic1(K, L) / num_batches
                hsic_matrix_svd[i, j, 2] += hsic1(L, L) / num_batches

        self.epoch_hsic_matrix_svd[epoch] = hsic_matrix_svd

    def batch(self, data_enum: Iterable, epoch: int):

        hsic_matrix = self.epoch_hsic_matrix.get(
            epoch, torch.zeros(len(self.capture_1), len(self.capture_2), 3)
        )

        num_batches = len(data_enum)

        for data, label in data_enum:
            data, label = data.to(self.device), label.to(self.device)
            # self.batch_svd(data, label, epoch, num_batches)

            for i, (layer_1, acts_1) in enumerate(self.capture_1(data).items()):

                K = tilde(acts_1)
                hsic_matrix[i, :, 0] += hsic1(K, K) / num_batches

                for j, (layer_2, acts_2) in enumerate(self.capture_2(data).items()):
                    L = tilde(acts_2)

                    hsic_matrix[i, j, 1] += hsic1(K, L) / num_batches
                    hsic_matrix[i, j, 2] += hsic1(L, L) / num_batches
        hsic_matrix = hsic_matrix[:, :, 1] / (hsic_matrix[:, :, 0].sqrt() * hsic_matrix[:, :, 2].sqrt())

        # hsic_matrix_svd = self.epoch_hsic_matrix_svd[epoch]
        # hsic_matrix_svd = hsic_matrix_svd[:, :, 1] / (
        #     hsic_matrix_svd[:, :, 0].sqrt() * hsic_matrix_svd[:, :, 2].sqrt()
        # )

        assert not torch.isnan(hsic_matrix).any()
        # assert not torch.isnan(hsic_matrix_svd).any()

        self.epoch_hsic_matrix[epoch] = hsic_matrix
        # self.epoch_hsic_matrix_svd[epoch] = hsic_matrix_svd


def _mp_helper(prev_layer, layer):
    _cka = cka_u_s(*prev_layer.values(), *layer.values())
    return _cka


class CKA:
    def __init__(
        self,
        model1: nn.Module,
        model2: nn.Module,
        device: str = "cuda",
    ):
        """
        :param model1: (nn.Module) Neural Network 1
        :param model2: (nn.Module) Neural Network 2
        :param model1_name: (str) Name of model 1
        :param device: Device to run the model
        """
        self._hooks_setup = False

        self.model1 = model1
        self.model2 = model2

        self.device = device

        self.model1 = self.model1.to(self.device)
        self.model2 = self.model2.to(self.device)

        self.model1.eval()
        self.model2.eval()

        self.hsic_matrix = None
        self.hsic_matrix_perturbed = None

    def cka_full(self, acts1: torch.Tensor, acts2: torch.Tensor, center: bool = True):
        if len(acts1.shape) > 2:
            acts1 = acts1.flatten(1)
        if len(acts2.shape) > 2:
            acts2 = acts2.flatten(1)

        if center:
            acts1 = acts1 - acts1.mean() / acts1.std()
            acts2 = acts2 - acts2.mean() / acts2.std()

        K = acts1 @ acts1.T
        L = acts2 @ acts2.T

        return hsic0(K, L) / torch.sqrt(hsic0(K, K) * hsic0(L, L))

    def compare_hsic(self, dataloader: DataLoader, perturber: Perturber):
        capture_model1 = CaptureManager(self.model1).capture(keep_activations=False)
        capture_model2 = CaptureManager(self.model2).capture(keep_activations=False)

        N = len(capture_model1)
        M = len(capture_model2)

        self.hsic_matrix = torch.zeros(N, M, 3)
        self.hsic_matrix_perturbed = torch.zeros(N, M)
        num_batches = len(dataloader)

        data_perturbed = None

        pbar = tqdm(enumerate(dataloader), desc="[comparing HSIC]", total=len(dataloader), position=1)
        with capture_model1 as cap1, capture_model2 as cap2:
            for (data_idx, (data, *_)) in pbar:
                data = data.to(self.device)
                if data_idx == 0:
                    if data_perturbed is None:
                        data_perturbed = perturber.repeat(data[0])
                    acts1_perturbed, _ = cap1(data_perturbed)
                    acts2_perturbed, _ = cap2(data_perturbed)

                    pbar.set_postfix({"section": "perturbation"})

                for i, (_, layer_acts1) in enumerate(acts1_perturbed.items()):
                    for j, (_, layer_acts2) in enumerate(acts2_perturbed.items()):
                        self.hsic_matrix_perturbed[i, j] = self.cka_full(layer_acts1, layer_acts2)

                pbar.set_postfix({"section": "minibatches"})

                acts1, _ = cap1(data)
                acts2, _ = cap2(data)

                for i, (_, layer_acts1) in enumerate(acts1.items()):

                    K = tilde(layer_acts1)
                    self.hsic_matrix[i, :, 0] += hsic1(K, K) / num_batches

                    for j, (_, layer_acts2) in enumerate(acts2.items()):
                        L = tilde(layer_acts2)

                        assert K.shape == L.shape, f"Feature shape mistach! {K.shape}, {L.shape}"
                        self.hsic_matrix[i, j, 1] += hsic1(K, L) / num_batches
                        self.hsic_matrix[i, j, 2] += hsic1(L, L) / num_batches

        self.hsic_matrix = self.hsic_matrix[:, :, 1] / (
            self.hsic_matrix[:, :, 0].sqrt() * self.hsic_matrix[:, :, 2].sqrt()
        )
        assert not torch.isnan(self.hsic_matrix).any(), "HSIC computation resulted in NANs"

    def _perturbed_cka(self, acts1: Dict[str, torch.Tensor], acts2: Dict[str, torch.Tensor]) -> None:
        pass
