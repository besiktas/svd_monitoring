import torch


def positivedef_matrix_sqrt(arr):
    w, v = torch.linalg.eigh(arr)
    wsqrt = torch.sqrt(w)
    sqrtarray = torch.matmul(v, torch.matmul(torch.diag(wsqrt), torch.conj(v).T))
    return sqrtarray


def get_cca_similarity(x, y, epsilon=0.0):
    num_x = len(x)
    num_y = len(y)

    if len(x.shape) > 1:
        x = x.view(num_x, -1)
    if len(y.shape) > 1:
        y = y.view(num_y, -1)

    covariance = torch.cov(torch.concat((x, y)))
    sigmaxx = covariance[:num_x, :num_x]
    sigmaxy = covariance[:num_x, num_x:]

    sigmayx = covariance[num_x:, :num_x]
    sigmayy = covariance[num_x:, num_x:]

    xmax, ymax = torch.max(torch.abs(sigmaxx)), torch.max(torch.abs(sigmayy))

    sigmaxx /= xmax
    sigmayy /= ymax
    sigmaxy /= torch.sqrt(xmax * ymax)
    sigmayx /= torch.sqrt(xmax * ymax)

    # compute cca
    # breakpoint()
    num_sigmaxx = len(sigmaxx)
    num_sigmayy = len(sigmayy)
    sigmaxx += epsilon * torch.eye(num_sigmaxx).to(sigmaxx.device)
    sigmayy += epsilon * torch.eye(num_sigmayy).to(sigmayy.device)

    inv_xx = torch.linalg.pinv(sigmaxx).to(sigmaxx.device)
    inv_yy = torch.linalg.pinv(sigmayy).to(sigmayy.device)

    invsqrt_xx = positivedef_matrix_sqrt(inv_xx)
    invsqrt_yy = positivedef_matrix_sqrt(inv_yy)

    arr = torch.matmul(invsqrt_xx, torch.matmul(sigmaxy, invsqrt_yy))
    u, s, v = torch.pca_lowrank(arr)
    return u, s, v
