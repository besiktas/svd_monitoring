from setuptools import setup, find_packages

setup(
    name="svd_monitoring",
    version="0.0.0",
    packages=find_packages(include=["experiments"]),
)
